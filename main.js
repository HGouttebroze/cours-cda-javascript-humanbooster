// !!! quelques RAPPELS JavaScript pour prepa.module CDA Human Booster:
// Mini-exercice de rappel:
/* 
  GESTION SOURIS JavaScript

  TAGS: 
  DOM, addEventListener, Event ('click', 'dblclick', 'mouseover', 'mouseout'), function(){}, querySelector
  classList.add(''): ajouter une class
  classList.toogle('') : ajouter ou supprimer une class 
  classList.remove('') : supprimer une class
  this : dans les écouteurs, fait référence à l'event écouté */

// Le HTML contient une balise ayant une apparence de rectangle dont la couleur change :
// - par défaut, il est en bleu
// - lorsque la souris passe dessus, il devient rose (sans utiliser la pseudo-classe `:hover`, on utilise le JS !)
// - lorsqu'on double clique dessus, il devient blanc

// Le changement de couleur se fait en changeant la classe HTML du 
// rectangle (`.important` pour passer en `#fc466b` au survol et `.good` pour passer en 
// `#ffe4c4` au double clic sur le rectangle).

/* Recommandations :

1. Prendre connaissance du fichier CSS
2. Cibler le bouton et le rectangle
3. Installer un [Gestionnaire d'événement](https://www.notion.so/Gestionnaire-d-v-nement-48c708c7a4954c5d8ddd9b9627e49ad3) sur le bouton qui appelle une fonction `onClickButton()`
4. Pour s'assurer que le gestionnaire d'événement fonctionne bien, mettre un `console.log()` dans la fonction : si le console log apparait, c'est que le gestionnaire d'événement fonctionne bien et on peut donc passer à la suite :
5. Ajouter ou supprimer la classe `hide` du bouton.
6. Installer un gestionnaire d'événement sur le rectangle, au survol de ce dernier qui appelle une fonction `onMouseOverRectangle()`
7. Modifier la classe du rectangle dans la fonction `onMouseOverRectangle()` pour lui ajouter `important`
8. Une fois que ça marche, on fait la même chose pour quand la souris sort du rectangle : on créé un gestionnaire d'événement qui appelle une fonction `onMouseOutRectangle()` qui a pour vocation de supprimer la classe `important` au rectangle
9. Ajouter un gestionnaire d'événement sur le rectangle qui appelle, au double click, une fonction `onDoubleClickRectangle()`
10. Cette fonction a pour but d'ajouter la classe `good` au rectangle
11. Modifier la fonction `onMouseOutRectangle()` pour qu'elle supprime également la classe `good` du rectangle lorsque la souris en sort
 */

// // je cible le button & le rectangle
// let x = document.querySelector('#toggle-rectangle')
// let y = document.querySelector('.rectangle')

// x.addEventListener('click', function onClickButton() {
//     //console.log('ok')
//     y.classList.toggle('hide')
// })

// // là je vais changer la couleur bleu pour la couleur rose ammené du rectangle avec la class 'important' sur un nouvelle fonction que je vais créer
// y.addEventListener('mouseover', function onMouseOverRectangle() { // au survol de la souris (event = 'mouseover')
//     this.classList.add('important')
// })

// // là je vais enlever la couleur blanche ammené au dble-click avec la class good sur la fonction onDoubleClickRectangle
// y.addEventListener('mouseout', function onMouseOverRectangle() { // quand la souris sort du rectangle (event = 'mouseout')
//   this.classList.remove('important')  
// })

// // là je vais changer la couleur du rectangle au dble-click (avec l'event 'dblclick') avec la class good sur la fonction onDoubleClickRectangle
// y.addEventListener('dblclick', function onDoubleClickRectangle() {
//     this.classList.add('good')
// })

// // là je vais enlever la couleur blanche ammené au dble-click avec la class good sur la fonction onDoubleClickRectangle
// y.addEventListener('click', function onClickRectangle() {
//     this.classList.remove('good')
// })

// // et j'enleve aussi la class .good sur l'event 'mouseout' avec en parametre une nouvelle fonction
// y.addEventListener('mouseout', function onMouseOutRectangle() {
//     this.classList.remove('good')
// })

function onClickButton(){
    //console.log('ok pr click')
    rectangle.classList.toggle('hide')
}
function onMouseOverRectangle(){
    rectangle.classList.add('important')
}
function onMouseOutRectangle(){
    rectangle.classList.remove('important')
    rectangle.classList.remove('good')
}
function onDoubleClickRectangle(){
    rectangle.classList.toggle('good')
}

const bouton = document.querySelector('#toggle-rectangle')
const rectangle = document.querySelector('.rectangle')

bouton.addEventListener('click', onClickButton)
rectangle.addEventListener('mouseover', onMouseOverRectangle)
rectangle.addEventListener('mouseout', onMouseOutRectangle)
rectangle.addEventListener('dblclick', onDoubleClickRectangle)



